import os
from matrix_admin_toolkit import MatrixAdminToolkit

matrix_admin_toolkit: MatrixAdminToolkit = MatrixAdminToolkit(user_id=os.environ.get('USER_ID', None),
                                                              password=os.environ.get('PASSWORD', None))

# All user media
# all_user_media: list = matrix_admin_toolkit.get_all_user_media(matrix_admin_toolkit.user_id)
# print(len(all_user_media))

# Information about user
# user_info: dict = matrix_admin_toolkit.get_user_info(matrix_admin_toolkit.user_id)
# print(user_info)

# Delete media
# result: bool = matrix_admin_toolkit.delete_media(input('Media ID: '))
# print(result)

# Get all users
# all_users: list = matrix_admin_toolkit.get_all_users()
# for user in all_users:
#     print(user)

# Remove all media to fixed size
# matrix_admin_toolkit.remove_media_to_a_certain_size(500, safe_avatar=True)

# Logout and exit work
# matrix_admin_toolkit.logout()
