import datetime

from .base_api import MatrixAPI, ApiError


class MatrixAdminToolkit(MatrixAPI):

    def __init__(self, user_id: str = None, password: str = None):
        super().__init__(user_id=user_id, password=password)

    # Control space in server function
    def remove_media_to_a_certain_size(self, size_per_user_Mb: int, white_list: list = [], safe_avatar: bool = True):
        """Delete all user files that exceed the user's disk space limit"""

        all_users: list = self.get_all_users()

        media_for_delete: list = []

        for user in all_users:
            if user['name'] not in white_list:
                user_media: list = self.get_all_user_media(user['name'])
                user_media.reverse()

                avatar_id: str = '' if user['avatar_url'] is None else \
                    user['avatar_url'].replace(f'mxc://{self.server_url.replace("https://", "")}/', '')

                all_media_size: int = 0

                for media in user_media:
                    all_media_size += media['media_length']

                if all_media_size/1000000 > size_per_user_Mb:
                    for media in user_media:
                        if safe_avatar and media['media_id'] == avatar_id:
                            continue

                        media_for_delete.append(media['media_id'])
                        all_media_size -= media['media_length']

                        if all_media_size / 1000000 < size_per_user_Mb:
                            break

        for media_id in media_for_delete:
            self.delete_media(media_id=media_id)
