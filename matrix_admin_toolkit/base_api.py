import os
import requests


class ApiError(Exception):
    reason: str = 'Unknown'

    def __init__(self, reason: str):
        self.reason = reason

    def __str__(self):
        return self.reason


class MatrixAPI:
    """Use Matrix API v1.2"""

    _access_token: str
    server_url: str

    user_id = str
    device_id = str

    endpoints: dict = {
        # All
        'login': '/_matrix/client/r0/login',
        'whoami': '/_matrix/client/v3/account/whoami',

        # Synapse admin
        'all_users': '/_synapse/admin/v2/users',
        'user_info': '/_synapse/admin/v2/users/{user_id}',
        'logout': '/_synapse/admin/v2/users/{user_id}/devices/{device_id}',
        'user_media': '/_synapse/admin/v1/users/{user_id}/media',
        'delete_media': '/_synapse/admin/v1/media/{server_name}/{media_id}',
    }

    headers: dict = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }

    def __init__(self, user_id: str = None, password: str = None):
        self._access_token = os.environ.get('ACCESS_TOKEN', None)
        self.server_url = os.environ.get('SERVER_URL', None)

        if self.server_url is None:
            raise ApiError('SERVER_URL not in environment')

        # Validate the access_token
        if not self._validate_access_token():
            if user_id is None and password is None:
                raise ApiError('ACCESS_TOKEN invalid and user_id is missing')
            else:
                if self.login(user_id=user_id, password=password):
                    self._validate_access_token()  # Need for update the device_id and user_id
                else:
                    raise ApiError('user_id login failed')

        # Update headers (add an access_token)
        self.headers.update({'Authorization': f'Bearer {self._access_token}'})

    def _validate_access_token(self) -> bool:
        """Validate access_token, return status"""

        headers: dict = {'Authorization': f'Bearer {self._access_token}'}
        headers.update(self.headers)

        response: requests.Response = requests.get(self.server_url + self.endpoints['whoami'], headers=headers)

        if response.ok:
            content: dict = response.json()

            self.user_id = content['user_id']
            self.device_id = content['device_id']

            return True

        elif response.status_code == 401:
            return False

        else:
            raise ApiError('Request error in _validate_access_token function')

    def login(self, user_id: str, password: str) -> bool:
        """Login or renew an access_token, return status"""

        payload: dict = {
            'type': 'm.login.password',
            'identifier': {
                'type': 'm.id.user',
                'user': user_id
            },
            'password': password
        }

        response = requests.post(self.server_url + self.endpoints['login'], json=payload, headers=self.headers)

        if response.ok:
            content: dict = response.json()

            self._access_token = content['access_token']

            return True
        else:
            return False

    def logout(self) -> None:
        """Deletes the device from the user account after work, use this to avoid chaos, please"""

        response: requests.Response = requests.delete(self.server_url + self.endpoints['logout']
                                                      .format(user_id=self.user_id,
                                                              device_id=self.device_id),
                                                      headers=self.headers)

        if not response.ok:
            raise ApiError('Request error in logout function')

    def get_all_user_media(self, user_id: str, all_user_media: list = [], next_token: int = 0) -> list:
        """Get links of all user media"""

        if next_token == 0:
            response = requests.Response = requests.get(self.server_url + self.endpoints['user_media']
                                                        .format(user_id=user_id),
                                                        headers=self.headers)
        else:
            response = requests.Response = requests.get(self.server_url + self.endpoints['user_media']
                                                        .format(user_id=user_id) + f'?from={next_token}',
                                                        headers=self.headers)

        if response.ok:
            content: dict = response.json()

            all_user_media = all_user_media.copy()

            all_user_media += content['media']

            if content.get('next_token', False):
                all_user_media = self.get_all_user_media(user_id=user_id,
                                                         all_user_media=all_user_media,
                                                         next_token=content['next_token'])

            return all_user_media
        else:
            raise ApiError('Request error in get_all_user_media function')

    def get_all_users(self) -> list:
        """Get all users data"""

        response: requests.Response = requests.get(self.server_url + self.endpoints['all_users'],
                                                   headers=self.headers)

        if response.ok:
            content: dict = response.json()

            return content['users']
        else:
            raise ApiError('Request error in get_users function')

    def get_user_info(self, user_id) -> dict:
        """Get info about user"""

        response: requests.Response = requests.get(self.server_url + self.endpoints['user_info']
                                                   .format(user_id=user_id),
                                                   headers=self.headers)

        if response.ok:
            content: dict = response.json()

            return content

        else:
            raise ApiError('Request error in get_user_info function')

    def delete_media(self, media_id: str) -> bool:
        """Delete media by ID"""

        response: requests.Response = requests.delete(self.server_url + self.endpoints['delete_media']
                                                      .format(server_name=self.server_url.replace('https://', ''),
                                                              media_id=media_id),
                                                      headers=self.headers)

        if response.ok:
            return True
        else:
            return False
