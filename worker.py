import asyncio
import sys
import yaml
from matrix_admin_toolkit import MatrixAdminToolkit
from loguru import logger


class MatrixAdminToolkitError(Exception):
    reason: str = 'Unknown'

    def __init__(self, reason: str):
        self.reason = reason

    def __str__(self):
        return self.reason


config_example: dict = {
    'cleaner': {
        'status': True,
        'timeout': 6 * 60,  # in minutes

        # Groups admins and users is required!
        # Specify the maximum size allowed in descending order: 0, 1024, 600, 20, ...
        'groups': [
            {
                'name': 'admins',
                'max_size_in_mb': 0,  # Zero if no limit
                'safe_avatar': True,
                'members': ['@admin:some_matrix.com']
            },
            {
                'name': 'donors',
                'max_size_in_mb': 1024,
                'safe_avatar': True,
                'members': ['@donor:some_matrix.com']
            },

            # Users don't need members list
            {
                'name': 'users',
                'max_size_in_mb': 600,
                'safe_avatar': True
            }
        ]
    }
}

matrix_admin_toolkit: MatrixAdminToolkit = MatrixAdminToolkit()


async def cleaner():
    if config['cleaner']['status']:
        logger.info('cleaner loaded')

        while True:
            logger.info('cleaner started')

            white_list: list = []

            for group in config['cleaner']['groups']:
                group: dict

                logger.info(f'cleaner: {group["name"]} started')

                if group['max_size_in_mb'] == 0:
                    white_list += group.get('members', [])
                    logger.info(f'cleaner: {group["name"]} stopped')
                    continue

                matrix_admin_toolkit.remove_media_to_a_certain_size(size_per_user_Mb=group['max_size_in_mb'],
                                                                    white_list=white_list.copy(),
                                                                    safe_avatar=group.get('safe_avatar', True))

                white_list += group.get('members', [])

                logger.info(f'cleaner: {group["name"]} stopped')

            logger.info('cleaner sleep')
            await asyncio.sleep(config['cleaner']['timeout'] * 60)


async def main():
    await asyncio.gather(cleaner(), )


def save_example_config():
    with open('config_example.yaml', 'w') as file:
        yaml.safe_dump(config_example, file)


# Якось потім :D
# def check_config(config: dict) -> bool:
#     """Check config file. Does not give a 100% guarantee."""
#
#     # Check cleaner
#
#     if config.get('cleaner', False):
#         cleaner: dict = config['cleaner']
#
#
#         if all((cleaner.get('status', False), cleaner.get('timeout', False), cleaner.get('groups', False))):
#             cleaner_groups: list = cleaner['groups']
#             admins_group_exist: bool = False
#             users_group_exist: bool = False
#
#             for group in cleaner_groups:
#                 group: dict
#
#                 if group.get('name', False):
#                     if group['name'] == 'admins':
#                         admins_group_exist = True
#                     if group['name'] == 'users':
#                         users_group_exist = True
#                 else:
#                     return False
#
#                 if not group.get('max_size_in_mb', False):
#                     return False
#
#                 if not group.get('members', False) and group['name'] != 'users':
#                     return False
#
#             if not all((admins_group_exist, users_group_exist)):
#                 return False
#         else:
#             return False
#     else:
#         return False
#
#     return True


if __name__ == '__main__':
    # save_example_config()

    with open('config.yaml', 'r') as file:
        config: dict = yaml.safe_load(file)

    asyncio.run(main())

    # if not check_config(config=config):
    #     raise MatrixAdminToolkitError('Config is invalid')
