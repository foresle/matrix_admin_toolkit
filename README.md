# Matrix Admin Toolkit

<img src="logo.png" align="right" width="140">

> The program is designed to simplify and automate some tasks that are on the admin and do not have a built-in solution in the server.

> Work in synapse now, API v1.2

## Operating modes

### Cleaner

Periodic cleaning of custom files depending on the permitted storage per user of a certain group.

`config.yaml`
```yaml
cleaner:
  # Groups admins and users is required!
  # Specify the maximum size allowed in descending order: 0, 1024, 600, 20, ...
  groups:
  - max_size_in_mb: 0 # Zero if no limit
    safe_avatar: true
    members:
    - '@admin:some_matrix.com'
    name: admins
  - max_size_in_mb: 1024
    safe_avatar: true
    members:
    - '@donor:some_matrix.com'
    name: donors

  # Users don't need members list
  - max_size_in_mb: 600
    safe_avatar: true
    name: users
  status: true
  timeout: 360 # in minutes

```

## How to start

You need to write a systemd service that would call the run.sh file with such environment changes.

```
ACCESS_TOKEN=<access_token>;
PASSWORD=<password>;
SERVER_URL=https://your_homeserver_url.com;
USER_ID=<user_id>;
```